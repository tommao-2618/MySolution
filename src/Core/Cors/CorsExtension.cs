﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public static class CorsExtension
    {
        private static string myAllowSpecificOrigins = "myAllowSpecificOrigins";

        public static IServiceCollection AddCorsService(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    myAllowSpecificOrigins,
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                    });
            });
            return services;
        }

        public static IApplicationBuilder UseCorsService(this IApplicationBuilder app)
        {
            app.UseCors(myAllowSpecificOrigins);
            return app;
        }
    }
}
