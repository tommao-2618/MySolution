﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class ExecuteResult
    {
        public bool IsSucceed { get; set; } = false;

        public string Message { get; set; } = string.Empty;

        public ExecuteResult SetSuccess()
        {
            IsSucceed = true;
            Message = string.Empty;
            return this;
        }

        public ExecuteResult SetFialMessage(string msg)
        {
            IsSucceed = false;
            Message = msg;
            return this;
        }
    }
}
