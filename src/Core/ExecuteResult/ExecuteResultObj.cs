﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class ExecuteResultObj<T>
    {
        public bool IsSucceed { get; set; } = false;

        public string Message { get; set; } = string.Empty;

        public T Data { get; set; }

        public ExecuteResultObj<T> SetSuccess(T data)
        {
            return Set(true, string.Empty, data);
        }

        public ExecuteResultObj<T> SetFailMessage(string message)
        {
            return Set(false, message, default);
        }

        private ExecuteResultObj<T> Set(bool isSucceed, string message, T data)
        {
            IsSucceed = isSucceed;
            Message = message;
            Data = data;
            return this;
        }
    }
}
