﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Core
{
    public class CustomContractResolver : CamelCasePropertyNamesContractResolver
    {
        /// <summary>
        /// 实现首字母小写
        /// </summary>
        /// <param name="propertyName">propertyName</param>
        /// <returns></returns>
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.Substring(0, 1).ToLower() + propertyName.Substring(1);
        }

        /// <summary>
        /// 对长整型做处理
        /// </summary>
        /// <param name="objectType">objectType</param>
        /// <returns></returns>
        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            if (objectType == typeof(long))
            {
                return new JsonConverterLong();
            }
            if (objectType == typeof(DateTime))
            {
                return new JsonConverterDateTime();
            }

            return base.ResolveContractConverter(objectType);
        }
    }
}
