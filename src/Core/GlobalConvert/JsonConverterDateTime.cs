﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Core
{
    public class JsonConverterDateTime : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if ((reader.ValueType == null || reader.ValueType == typeof(DateTime?)) && reader.Value == null)
            {
                return null;
            }
            else
            {
                DateTime.TryParse(reader.Value != null ? reader.Value.ToString() : string.Empty, out DateTime value);
                return value;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteValue(value);
            }
            else
            {
                value = ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
                writer.WriteValue(value + string.Empty);
            }
        }
    }
}
