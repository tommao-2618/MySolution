﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Core
{
    /// <summary>
    /// Long类型Json序列化重写
    /// 在js中传输会导致精度丢失，故而在序列化时转换成字符类型
    /// </summary>
    public class JsonConverterLong : JsonConverter
    {
        /// <summary>
        /// 是否可以转换
        /// </summary>
        /// <param name="objectType">objectType</param>
        /// <returns>bool</returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// 读json
        /// </summary>
        /// <param name="reader">1</param>
        /// <param name="objectType">1.</param>
        /// <param name="existingValue">1.</param>
        /// <param name="serializer">1</param>
        /// <returns>1</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if ((reader.ValueType == null || reader.ValueType == typeof(long?)) && reader.Value == null)
            {
                return null;
            }
            else
            {
                long.TryParse(reader.Value != null ? reader.Value.ToString() : string.Empty, out long value);
                return value;
            }
        }

        /// <summary>
        /// 写json
        /// </summary>
        /// <param name="writer">1</param>
        /// <param name="value">1.</param>
        /// <param name="serializer">1</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteValue(value);
            }
            else
            {
                writer.WriteValue(value + string.Empty);
            }
        }
    }
}