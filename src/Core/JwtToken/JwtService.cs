﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Core
{
    public class JwtService
    {
        private readonly JwtSetting _jwtSetting;
        public JwtService(IOptions<JwtSetting> options)
        {
            _jwtSetting = options.Value;
        }
        /// <summary>
        /// 生成身份信息
        /// </summary>
        /// <param name="userData">用户信息</param>
        /// <returns>身份信息</returns>
        public Claim[] BuildClaims(UserData userData)
        {
            // 配置用户标识
            var userClaims = new Claim[]
            {
                new Claim(UserClaimType.Name, userData.Name),
                new Claim(UserClaimType.Code, userData.Code.ToString()),
                new Claim(UserClaimType.RoleName, userData.RoleName),
                new Claim(UserClaimType.RoleType, userData.RoleType.ToString()),
            };
            return userClaims;
        }

        /// <summary>
        /// 生成jwt令牌
        /// </summary>
        /// <param name="claims">自定义的claim</param>
        /// <returns>token</returns>
        public string BuildToken(Claim[] claims)
        {
            var nowTime = DateTime.Now;
            var creds = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSetting.SecurityKey)), SecurityAlgorithms.HmacSha256);
            JwtSecurityToken tokenkey = new JwtSecurityToken(
                issuer: _jwtSetting.Issuer,
                audience: _jwtSetting.Audience,
                claims: claims,
                notBefore: nowTime,
                expires: nowTime.Add(TimeSpan.FromMinutes(_jwtSetting.LifeTime)),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(tokenkey);
        }
    }
}