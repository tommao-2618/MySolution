﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Core
{
    public class UserClaim
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserClaim(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public ClaimsPrincipal UserPrincipal
        {
            get
            {
                ClaimsPrincipal user = _httpContextAccessor.HttpContext.User;
                if (user.Identity.IsAuthenticated)
                {
                    return user;
                }
                else
                {
                    throw new Exception("用户未认证");
                }
            }
        }

        public long Code
        {
            get
            {
                return Convert.ToInt64(UserPrincipal.Claims.First(x => x.Type == UserClaimType.Code).Value);
            }
        }

        public string Name
        {
            get
            {
                return UserPrincipal.Claims.First(x => x.Type == UserClaimType.Name).Value;
            }
        }

        public string RoleName
        {
            get
            {
                return UserPrincipal.Claims.First(x => x.Type == UserClaimType.RoleName).Value;
            }
        }

        public int RoleType
        {
            get
            {
                return Convert.ToInt32(UserPrincipal.Claims.First(x => x.Type == UserClaimType.RoleType).Value);
            }
        }

        public UserData GetUserData()
        {
            UserData userData = new UserData()
            {
                Code = Code,
                Name = Name,
                RoleName = RoleName,
                RoleType = RoleType,
            };

            return userData;
        }
    }
}
