﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public static class UserClaimType
    {
        public const string Code = "jwtCode";
        public const string Name = "jwtName";
        public const string RoleName = "jwtRoleName";
        public const string RoleType = "jwtRoleType";
    }
}
