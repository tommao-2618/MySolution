﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class UserData
    {
        /// <summary>
        /// 编码
        /// </summary>
        public long Code { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// 角色类型
        /// </summary>
        public int RoleType { get; set; }
    }
}
