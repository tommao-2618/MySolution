﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Core
{
    public static class PagedExtension
    {
        public static Paged<T> ToPaged<T>(this IEnumerable<T> source, int pageIndex, int pageSize)
        {
            Paged<T> pagedList = new Paged<T>();
            if (source is IQueryable<T> querable)
            {
                pagedList.PageIndex = pageIndex;
                pagedList.PageSize = pageSize;
                pagedList.TotalCount = querable.Count();
                pagedList.TotalPages = (int)Math.Ceiling(pagedList.TotalCount / (double)pagedList.PageSize);

                pagedList.Items = querable.Skip((pagedList.PageIndex - 1) * pagedList.PageSize).Take(pagedList.PageSize).ToList();
            }
            else
            {
                pagedList.PageIndex = pageIndex;
                pagedList.PageSize = pageSize;
                pagedList.TotalCount = source.Count();
                pagedList.TotalPages = (int)Math.Ceiling(pagedList.TotalCount / (double)pagedList.PageSize);

                pagedList.Items = source.Skip((pagedList.PageIndex - 1) * pagedList.PageSize).Take(pagedList.PageSize).ToList();
            }
            return pagedList;
        }
    }
}
