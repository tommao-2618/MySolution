﻿using Microsoft.Extensions.Logging;
using Quartz;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class RegularWork : IJob
    {
        private readonly ILogger<RegularWork> _logger;
        private readonly IDatabase _redisDb;

        public RegularWork(
            ILogger<RegularWork> logger,
            RedisService redisService)
        {
            _logger = logger;
            _redisDb = redisService.GetDatabase();
        }

        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine("这是Quartz定时任务");
            var now = DateTime.Now;
            var key = now.ToString("yyyyMMdd:HHmmss");
            var value = now.ToString("yyyy-MM-dd HH:mm:ss");
            await _redisDb.StringSetAsync("key", "value", TimeSpan.FromMinutes(1));
        }
    }
}
