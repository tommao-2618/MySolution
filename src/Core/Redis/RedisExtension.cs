﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public static class RedisExtension
    {
        public static IServiceCollection AddRedisService(this IServiceCollection services, IConfiguration configuration)
        {
            // 连接字符串
            string connectionString = configuration.GetSection("Redis:Connection").Value;

            // 实例名称
            string instanceName = configuration.GetSection("Redis:InstanceName").Value;

            // 默认数据库
            int defaultDB = Convert.ToInt32(configuration.GetSection("Redis:DefaultDB").Value);

            services.AddSingleton(new RedisService(connectionString, instanceName, defaultDB));
            return services;
        }
    }
}
