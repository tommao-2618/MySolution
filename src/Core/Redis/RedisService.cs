﻿using System;
using System.Collections.Concurrent;
using StackExchange.Redis;

namespace Core
{
    public class RedisService : IDisposable
    {
        // 连接字符串
        private string _connectionString;

        // 实例名称
        private string _instanceName;

        // 默认数据库
        private int _defaultDB;

        private ConcurrentDictionary<string, ConnectionMultiplexer> _connections;

        public RedisService(string connectionString, string instanceName, int defaultDB = 0)
        {
            _connectionString = connectionString;
            _instanceName = instanceName;
            _defaultDB = defaultDB;
            _connections = new ConcurrentDictionary<string, ConnectionMultiplexer>();
        }

        /// <summary>
        /// 获取数据库
        /// </summary>
        /// <returns></returns>
        public IDatabase GetDatabase()
        {
            return GetConnect().GetDatabase(_defaultDB);
        }

        public IServer GetServer(string configName = null, int endPointsIndex = 0)
        {
            var confOption = ConfigurationOptions.Parse(_connectionString);
            return GetConnect().GetServer(confOption.EndPoints[endPointsIndex]);
        }

        /// <summary>
        /// 获取订阅服务
        /// </summary>
        /// <returns></returns>
        public ISubscriber GetSubscriber()
        {
            return GetConnect().GetSubscriber();
        }

        public void Dispose()
        {
            if (_connections != null && _connections.Count > 0)
            {
                foreach (var item in _connections.Values)
                {
                    item.Close();
                }
            }
        }

        private ConnectionMultiplexer GetConnect()
        {
            return _connections.GetOrAdd(_instanceName, p => ConnectionMultiplexer.Connect(_connectionString));
        }
    }
}
