﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Senparc.CO2NET;
using Senparc.CO2NET.AspNet;
using Senparc.CO2NET.RegisterServices;
using Senparc.Weixin;
using Senparc.Weixin.Cache.CsRedis;
using Senparc.Weixin.Entities;
using Senparc.Weixin.RegisterServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public static class SenparcExtension
    {
        public static IServiceCollection AddSenparcService(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddQuartzService();
            /*
            * CO2NET 是从 Senparc.Weixin 分离的底层公共基础模块，经过了长达 6 年的迭代优化，稳定可靠。
            * 关于 CO2NET 在所有项目中的通用设置可参考 CO2NET 的 Sample：
            * https://github.com/Senparc/Senparc.CO2NET/blob/master/Sample/Senparc.CO2NET.Sample.netcore/Startup.cs
            */
            services.AddSenparcGlobalServices(configuration).
                     AddSenparcWeixinServices(configuration); // Senparc.Weixin 注册（必须）
            return services;
        }

        public static IApplicationBuilder UseSenparcService(this IApplicationBuilder app, IWebHostEnvironment env, IOptions<SenparcSetting> senparcSetting, IOptions<SenparcWeixinSetting> senparcWeixinSetting)
        {
            app.UseSenparcGlobal(env, senparcSetting.Value, globalRegister =>
            {
                Senparc.CO2NET.Cache.CsRedis.Register.SetConfigurationOption(senparcSetting.Value.Cache_Redis_Configuration);

                // 以下会立即将全局缓存设置为 Redis
                Senparc.CO2NET.Cache.CsRedis.Register.UseKeyValueRedisNow(); // 键值对缓存策略（推荐）
            }, true)
            .UseSenparcWeixin(senparcWeixinSetting.Value, weixinRegister =>
            {
                weixinRegister.UseSenparcWeixinCacheCsRedis(); // CsRedis，两选一
            });
            return app;
        }
    }
}
