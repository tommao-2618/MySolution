﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Snowflake;

namespace Core
{
    public static class SnowflakeExtension
    {
        public static IServiceCollection AddSnowflakeService(this IServiceCollection services)
        {
            // IdWorker 应该以单实例模式运行，否则会出现重复Id。
            IdWorker idWorker = new IdWorker(1, 1);
            services.AddSingleton(idWorker);
            return services;
        }
    }
}
