﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DbAccess
{
    public static class DbAccessExtension
    {
        public static IServiceCollection AddDbAccessService<TContext>(this IServiceCollection services, System.Action<DbContextOptionsBuilder> action) where TContext : DbContext
        {
            // 注册dbcontext
            services.AddDbContext<TContext>(action);

            // 注册工作单元
            services.AddScoped<IUnitOfWork<TContext>, UnitOfWork<TContext>>();
            return services;
        }
    }
}
