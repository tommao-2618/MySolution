﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace DbAccess
{
    public interface IUnitOfWork<TContext> where TContext : DbContext
    {
        TContext DbContext { get; }

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        IDbContextTransaction BeginTransaction();

        int SaveChanges();

        Task<int> SaveChangesAsync();

        int ExecuteSqlCommand(string sql, params object[] parameters);

        IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class;
    }
}
