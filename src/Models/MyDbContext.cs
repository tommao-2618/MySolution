﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Models
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 表
            modelBuilder.Entity<User>().ToTable("user");
            modelBuilder.Entity<LogRecords>().ToTable("log_records");

            // 视图
            // HasNoKey，没有主键
            // modelBuilder.Entity<AdministratorInfoView>().HasNoKey().ToView("administrator_info_view");
        }
    }
}
