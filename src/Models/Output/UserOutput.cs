﻿using Npoi.Mapper.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class UserOutput
    {
        [Column("用户ID")]
        public string ID { get; set; }

        [Column("用户名")]
        public string Name { get; set; }

        [Column("用户年龄")]
        public int Age { get; set; }
    }
}
