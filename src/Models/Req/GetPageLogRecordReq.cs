﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class GetPageLogRecordReq : PageReq
    {
        public string TraceId { get; set; }

        public DateTime LogStartDate { get; set; }

        public DateTime LogEndDate { get; set; }

        public string LogLevel { get; set; }

        public string Message { get; set; }

        public string Exception { get; set; }

        public string NetRequestUrl { get; set; }
    }
}
