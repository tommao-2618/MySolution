﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class PageReq
    {
        /// <summary>
        /// 页码
        /// </summary>
        /// <example>1</example>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 页大小
        /// </summary>
        /// <example>10</example>
        public int PageSize { get; set; } = 10;

        public virtual (bool IsSuccess, string Message) Valid()
        {
            if (PageIndex <= 0)
            {
                return (false, "PageIndex不能小于0！");
            }

            if (PageSize <= 0 || PageSize > 50)
            {
                return (false, "PageSize必须在0-50之间！");
            }

            return (true, string.Empty);
        }
    }
}
