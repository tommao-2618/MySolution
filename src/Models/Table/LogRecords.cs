﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class LogRecords
    {
        public int Id { get; set; }

        public string TraceId { get; set; }

        public DateTime LogDate { get; set; }

        public string LogLevel { get; set; }

        public string Logger { get; set; }

        public string Message { get; set; }

        public string Exception { get; set; }

        public string MachineName { get; set; }

        public string MachineIp { get; set; }

        public string NetRequestMethod { get; set; }

        public string NetRequestUrl { get; set; }

        public string NetUserIsauthenticated { get; set; }

        public string NetUserAuthtype { get; set; }

        public string NetUserIdentity { get; set; }
    }
}
