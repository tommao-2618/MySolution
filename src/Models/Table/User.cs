﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class User
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }
    }

}
