﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Untils
{
    public static class ConvertHelper
    {
        public static T MapFrom<T, TF>(this T to, TF from)
        {
            var typedTo = typeof(T);
            var typeFrom = from.GetType();
            foreach (var f in typeFrom.GetProperties())
            {
                var temp = typedTo.GetProperties();
                var temp1 = typeFrom.GetProperties();
                foreach (var t in typedTo.GetProperties())
                {
                    if (t.Name == f.Name && t.PropertyType == f.PropertyType)
                    {
                        t.SetValue(to, f.GetValue(from, null), null);
                        break;
                    }
                }
            }

            return to;
        }

        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T ToObject<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
