﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Untils
{
    public static class HttpHelper
    {
		/// <summary>
		/// 发起 POST Json 请求
		/// </summary>
		/// <param name="factory">HttpClient工厂</param>
		/// <param name="url">请求地址</param>
		/// <param name="json">Json字符串</param>
		/// <param name="headers">请求头</param>
		/// <param name="timeOut">超时时间，默认5000ms</param>
		/// <param name="name">Client名</param>
		/// <param name="cancellationToken">cancellationToken</param>
		/// <returns>【(bool, string, string)(成功与否, 错误信息, 调用结果)】</returns>
		public static async Task<(bool Flag, string Msg, string Data)> DoPostJsonAsync(this IHttpClientFactory factory, string url, string json, Dictionary<string, string> headers = null, int timeOut = 5000, string name = "common", CancellationToken cancellationToken = default(CancellationToken))
		{
			HttpClient httpClient = factory.CreateClient(name);
			httpClient.Timeout = TimeSpan.FromMilliseconds(timeOut);
			StringContent stringContent = new StringContent(json);
			stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
			HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url)
			{
				Content = stringContent
			};
			if (headers != null)
			{
				foreach (KeyValuePair<string, string> header in headers)
				{
					if (!httpRequestMessage.Headers.TryGetValues(header.Key, out var _))
					{
						httpRequestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value);
					}
				}
			}
			HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage, cancellationToken);
			string text = await response.Content.ReadAsStringAsync();
			if (response.IsSuccessStatusCode)
			{
				return (true, string.Empty, text);
			}
			return (false, $"状态码({response.StatusCode}),内容({text})", string.Empty);
		}

		/// <summary>
		/// 发起 Get 请求
		/// </summary>
		/// <param name="factory">HttpClient工厂</param>
		/// <param name="url">请求地址</param>
		/// <param name="paramters">Url参数</param>
		/// <param name="headers">请求头</param>
		/// <param name="timeOut">超时时间，默认5000ms</param>
		/// <param name="name">Client名</param>
		/// <param name="cancellationToken">cancellationToken</param>
		/// <returns>【(bool, string, string)(成功与否, 错误信息, 调用结果)】</returns>
		public static async Task<(bool Flag, string Msg, string Data)> DoGetAsync(this IHttpClientFactory factory, string url, Dictionary<string, string> paramters = null, Dictionary<string, string> headers = null, int timeOut = 5000, string name = "common", CancellationToken cancellationToken = default(CancellationToken))
		{
			HttpClient httpClient = factory.CreateClient(name);
			httpClient.Timeout = TimeSpan.FromMilliseconds(timeOut);
			if (paramters != null)
			{
				List<string> list = new List<string>();
				foreach (KeyValuePair<string, string> paramter in paramters)
				{
					list.Add(paramter.Key + "=" + paramter.Value);
				}
				url = url + "?" + string.Join("&", list);
			}
			HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, url);
			if (headers != null)
			{
				foreach (KeyValuePair<string, string> header in headers)
				{
					if (!httpRequestMessage.Headers.TryGetValues(header.Key, out var _))
					{
						httpRequestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value);
					}
				}
			}
			HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage, cancellationToken);
			string text = await response.Content.ReadAsStringAsync();
			if (response.IsSuccessStatusCode)
			{
				return (true, string.Empty, text);
			}
			return (false, $"状态码({response.StatusCode}),内容({text})", string.Empty);
		}

		public static async Task WriteResultAsync(this HttpContext httpContext, int status, int code, string msg)
		{
			httpContext.Response.StatusCode = status;
			httpContext.Response.ContentType = "application/json; charset=utf-8";
			string text = JsonConvert.SerializeObject(new { code, msg });
			await httpContext.Response.WriteAsync(text);
		}

	}
}
