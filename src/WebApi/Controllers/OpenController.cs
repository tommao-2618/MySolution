﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Core;
using DbAccess;
using LianOu.Auth.SDK;
using LianOu.CoreLib.Http.Extensions;
using LianOu.FileLib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using MongoDB.Driver;
using Newtonsoft.Json;
using Snowflake;
using StackExchange.Redis;
using Untils;

namespace WebApi.Controllers
{
    [AllowAnonymous]
    public class OpenController : BaseController
    {
        private readonly ILogger<OpenController> _logger;
        private readonly IUnitOfWork<MyDbContext> _unitOfWork;
        private readonly IdWorker _idWorker;
        private readonly JwtService _jwtService;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IDatabase _redisDb;


        public OpenController(
            IUnitOfWork<MyDbContext> unitOfWork,
            ILogger<OpenController> logger,
            IdWorker idWorker,
            IHttpClientFactory httpClientFactory,
            JwtService jwtService,
            RedisService redisService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _idWorker = idWorker;
            _jwtService = jwtService;
            _httpClientFactory = httpClientFactory;
            _redisDb = redisService.GetDatabase();
        }

        /// <summary>
        /// 测试接口
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ExecuteResult), 200)]
        public IActionResult Index(string flag)
        {
            ExecuteResultObj<object> result = new ExecuteResultObj<object>();
            try
            {
                Task.Run(() => {
                    _logger.LogInformation("防爆001");
                    _logger.LogInformation("防爆002");
                    _logger.LogInformation("防爆003");
                    _logger.LogInformation("防爆004");
                });
               
            }
            catch (Exception ex)
            {
                result.SetFailMessage(ex.Message);
            }
            return Ok(result);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Login(string name)
        {
            ExecuteResultObj<object> result = new ExecuteResultObj<object>();
            UserData userData = new UserData()
            {
                Name = name,
                Code = 001,
                RoleType = 1,
                RoleName = "管理员"
            };
            string token = _jwtService.BuildToken(_jwtService.BuildClaims(userData));
            result.SetSuccess($"Bearer {token}");
            return Ok(result);

        }

        /// <summary>
        /// 获取日志列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetPageLogRecord(GetPageLogRecordReq req)
        {
            ExecuteResultObj<Paged<LogRecords>> result = new ExecuteResultObj<Paged<LogRecords>>();

            var query = PredicateBuilder.True<LogRecords>();
            if (!string.IsNullOrEmpty(req.TraceId)) query = query.And(m => m.TraceId.Contains(req.TraceId));
            if (!string.IsNullOrEmpty(req.Message)) query = query.And(m => m.Message.Contains(req.Message));
            if (!string.IsNullOrEmpty(req.Exception)) query = query.And(m => m.Exception.Contains(req.Exception));
            if (!string.IsNullOrEmpty(req.NetRequestUrl)) query = query.And(m => m.NetRequestUrl.Contains(req.NetRequestUrl));
            if (!string.IsNullOrEmpty(req.LogLevel)) query = query.And(m => m.LogLevel.Contains(req.LogLevel));
            if (req.LogStartDate != default) query = query.And(m => m.LogDate >= req.LogStartDate);
            if (req.LogEndDate != default) query = query.And(m => m.LogDate <= req.LogEndDate);


            var pageData = _unitOfWork.GetRepository<LogRecords>().DbSet.Where(query).OrderByDescending(m=>m.LogDate).ToPaged(req.PageIndex, req.PageSize);
            result.SetSuccess(pageData);
            return Ok(result);

        }
    }
}
