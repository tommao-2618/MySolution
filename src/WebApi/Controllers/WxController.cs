﻿using Core;
using LianOu.Auth.SDK;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Senparc.Weixin.Entities;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.WxOpen.AdvancedAPIs.Sns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Untils;

namespace WebApi.Controllers
{
    public class WxController : BaseController
    {

        private readonly SenparcWeixinSetting _senparcWeixinSetting;
        public WxController(
               IOptions<SenparcWeixinSetting> senparcWeixinSetting
            )
        {
            _senparcWeixinSetting = senparcWeixinSetting.Value;
        }

        /// <summary>
        /// 公共的获取access_token
        /// </summary>
        /// <returns>string</returns>
        [HttpGet]
        public async Task<IActionResult> GetAccessTokenAsync()
        {
            ExecuteResultObj<string> result = new ExecuteResultObj<string>();
            var appId = _senparcWeixinSetting.WxOpenAppId;
            var srcectId = _senparcWeixinSetting.WxOpenAppSecret;
            var token = await AccessTokenContainer.TryGetAccessTokenAsync(appId, srcectId);
            result.SetSuccess(token);
            return Ok(result);
        }
    }
}
