﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.Filters
{
    public class ApiResultFilter : IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext context)
        {
        }

        /// <summary>
        /// 响应拦截器
        /// </summary>
        /// <param name="context">context</param>
        public void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result != null)
            {
                if (context.Result is ObjectResult objectResult)
                {
                    // IActionResult类型
                    if (objectResult.DeclaredType is null)
                    {
                        context.Result = new JsonResult(new
                        {
                            status = objectResult.StatusCode,
                            data = objectResult.Value,
                        });
                    }

                    // String、List等类型
                    else
                    {
                        context.Result = new JsonResult(new
                        {
                            status = 200,
                            data = objectResult.Value,
                        });
                    }
                }
                else if (context.Result is EmptyResult)
                {
                    context.Result = new JsonResult(new
                    {
                        status = 200,
                        data = string.Empty,
                    });
                }
                else
                {
                    throw new Exception($"未经处理的Result类型：{context.Result.GetType().Name}");
                }
            }
        }
    }
}
