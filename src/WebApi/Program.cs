using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;

namespace WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Trace("WebApi启动中>>>>>");
            try
            {
                var host = CreateHostBuilder(args).Build();

                using (IServiceScope scope = host.Services.CreateScope())
                {
                    NLogExtension.EnsureNlogConfig("NLog.config", "MySQL", scope.ServiceProvider.GetRequiredService<IConfiguration>().GetSection("ConnectionStrings:MySql").Value);
                }

                logger.Trace("WebApi启动完成>>>>>");
                host.Run();
            }
            catch (Exception ex)
            {
                logger.Fatal("WebApi启动失败>>>>>");
                logger.Fatal(ex.Message);
                throw;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).AddNlogService();
    }
}
