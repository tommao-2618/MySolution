using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using DbAccess;
using LianOu.Auth.SDK.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Models;
using Senparc.CO2NET;
using Senparc.Weixin.Entities;
using WebApi.Filters;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(m =>
            {
                m.Filters.Add<ApiResultFilter>();
                m.Filters.Add<ApiExceptionFilter>();
                m.Filters.Add<ApiActionFilter>();
                m.Filters.Add<ApiAuthorizationFilter>();
            }).AddNewtonsoftJson(options =>
            {
                // 全局格式化json
                options.SerializerSettings.ContractResolver = new CustomContractResolver();
            });
            services.AddSwaggerService();
            services.AddDbAccessService<MyDbContext>(options => { options.UseMySql(Configuration.GetSection("ConnectionStrings:MySql").Value); });
            services.AddSnowflakeService();
            services.AddRedisService(Configuration);
            services.AddJwtService(Configuration);
            services.AddCorsService();
            services.AddHttpClient();
            services.AddSenparcService(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<SenparcSetting> senparcSetting, IOptions<SenparcWeixinSetting> senparcWeixinSetting)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerService();

            app.UseCorsService();

            app.UseSenparcService(env, senparcSetting, senparcWeixinSetting);
        }
    }
}
