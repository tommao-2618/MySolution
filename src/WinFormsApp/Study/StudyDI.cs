﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace WinFormsApp.Study
{
    public partial class StudyDI : Form
    {
        public StudyDI()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //根容器
            var root = new ServiceCollection()
                //瞬时：每次调用对应的服务都会实例化一次
                .AddTransient<IFirst, First>()
                //区域：在同一作用域内调用只会实例化一次，每个http请求会产生一个作用域ServiceProvider
                .AddScoped<ISecond, Second>()
                //单例：对象实例保存在根容器，在整个程序中实例化一次
                .AddSingleton<IThird, Third>()
                //实例化带参数的对象
                .AddSingleton(new Fourth("hello LNM"))
                .BuildServiceProvider();

            //所有子容器之间都是平级的，只能知道上级是根容器
            var child1 = root.CreateScope().ServiceProvider;
            var child2 = child1.CreateScope().ServiceProvider;

            GetTwoServices<IFirst>(child2);
            GetTwoServices<ISecond>(child2);
            GetTwoServices<IThird>(child2);
            Console.WriteLine();

            GetTwoServices<IFirst>(child1);
            GetTwoServices<ISecond>(child1);
            GetTwoServices<IThird>(child1);
            Console.WriteLine();

            GetTwoServices<IFirst>(root);
            GetTwoServices<ISecond>(root);
            GetTwoServices<IThird>(root);
            Console.WriteLine();

            GetTwoServices<Fourth>(root);
        }

        private void GetTwoServices<T>(IServiceProvider provider)
        {
            provider.GetService<T>();
            provider.GetService<T>();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
             * Autofac
             * 服务集合-容器构建对象(Autofac)-容器对象
             * 优势：属性注入、批量注册
             */
            var serviceCollection = new ServiceCollection()
                .AddSingleton<IFirst, First>()
                .AddScoped<ISecond, Second>();
            //容器构建对象
            var containerBuilder = new ContainerBuilder();
       
            containerBuilder.Populate(serviceCollection);
            //Autofac注入
            containerBuilder.RegisterType<Third>().As<IThird>();

            var container = containerBuilder.Build();

            var provider = new AutofacServiceProvider(container);

            GetTwoServices<IFirst>(provider);
            GetTwoServices<ISecond>(provider);
            GetTwoServices<IThird>(provider);
            Console.WriteLine();
        }
    }

    public class Base : IDisposable
    {
        public Base()
        {
            Console.WriteLine($"Created:{GetType().Name}");
        }

        public void Dispose()
        {
            Console.WriteLine($"Dispose:{GetType().Name}");
        }
    }

    public interface IFirst { }

    public class First : Base, IFirst { }

    public interface ISecond { }

    public class Second : Base, ISecond { }

    public interface IThird { }

    public class Third : Base, IThird { }

    public interface IFourth { }

    public class Fourth
    {
        public Fourth(string flag)
        {
            Console.WriteLine(flag);
        }
    }
}
