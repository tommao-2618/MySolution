﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp.Study
{
    public delegate void FirstDelegate(int index);

    public delegate string SecondDelegate(string index);

    public delegate T ThirdDelegate<T>(T index);

    public delegate int FourthDelegate();
    public partial class StudyDelegate : Form
    {
        public StudyDelegate()
        {
            InitializeComponent();
        }

        public int Index { get; set; }

        public void Test(int index)
        {
            Console.WriteLine($"this is {index}");
        }

        public int AddNum()
        {
            return Index++;
        }

        public int DoubleNum()
        {
            Index = Index * 2;
            return Index;
        }

        public string Do(int x)
        {
            return $"this is {x}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
           * 1、委托使用delegate在方法上声明，本质上委托是类。
           *   1.1 委托可以声明在类的内部，也可以在命名空间。
           * 2、操作目标是方法。
           *   2.1 委托的签名必须和传进的方法参数一样。
           * 3、委托的作用。
           *   3.1 委托可以代理目标对象的所有方法。
           * 4、多播委托
           */

            FirstDelegate firstDelegate = new FirstDelegate(Test);
            firstDelegate(4444);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // 多播委托就是将委托像链表一样串联起来，进行相加减
            // 根据委托产生的匿名方法
            FirstDelegate firstDelegate1 = x => Console.WriteLine(x);
            FirstDelegate firstDelegate2 = x => Console.WriteLine(x);
            FirstDelegate firstDelegate3 = firstDelegate1 + firstDelegate2;
            firstDelegate3(1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Action内置委托可以通过Acion<T>传递参数，Action委托无返回值。
            Action action = () => Console.WriteLine("this is action");
            action();
            Action<string> p = (x) => Console.WriteLine(x);
            p("11111111");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
          与C#-Action十分相似， Func<> 也是c#内置的委托类型，
          不同的是， Func<> 只能用来引用具有返回值的方法,也就是说，
          在使用它时，至少需要在尖括号内指定一种类型，当仅指定一种类型时，
          表示引用具有返回值但没有参数的方法，当指定多种类型时，
          其中最后一个类型表示返回值类型，前面的表示所引用方法的参数列表的类型。
          */
            Func<int, string> p = (x) => { return $"this is {x}"; };
            Console.WriteLine(p(2));
        }
    }
}
