﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WinFormsApp.StudyData;

namespace WinFormsApp.Study
{
    public partial class StudyDesignPattern : Form
    {
        public StudyDesignPattern()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //单例模式就是一个类只能实例化一次，就是在类内部实例化，外部无法对该类实例化。
            var singleton = Singleton.GetSingleton();
            var safeSingleton = ThreadSafeSingleton.GetThreadSafeSingleton();

            Console.WriteLine(singleton.MyProperty);
            Console.WriteLine(safeSingleton.MyProperty);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IAutoCarMake autoCarMake = new Factory().FactoryCreateCar("red");
            autoCarMake.CreateAutoCar();
        }
    }
}
