﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WinFormsApp.StudyData;
using System.Configuration;

namespace WinFormsApp.Study
{
    public partial class StudyFileSystem : Form
    {
        public StudyFileSystem()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var provider = new ServiceCollection()
                .AddSingleton<IFileProvider>(new PhysicalFileProvider(@"E:\Test"))
                .AddSingleton<StudyFileSystemData>()
                .BuildServiceProvider();

            var studyFileSystemData = provider.GetService<StudyFileSystemData>();

            studyFileSystemData.Dir();
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            var provider = new ServiceCollection()
               .AddSingleton<IFileProvider>(new PhysicalFileProvider(@"E:\Test"))
               .AddSingleton<StudyFileSystemData>()
               .BuildServiceProvider();

            var studyFileSystemData = provider.GetService<StudyFileSystemData>();

            var content = await studyFileSystemData.ReadAsync("新建文本文档.txt");

            Console.WriteLine(content);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var provider = new ServiceCollection()
            .AddSingleton<IFileProvider>(new PhysicalFileProvider(@"E:\Test"))
            .AddSingleton<StudyFileSystemData>()
            .BuildServiceProvider();

            var studyFileSystemData = provider.GetService<StudyFileSystemData>();

            //会通过一个OnChange函数监控文件内容变化
            studyFileSystemData.WatchAsync("新建文本文档.txt");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
             配置模型的3个核心对象：
            1.配置源对象：IConfigurationSource
            2.配置构建对象：IConfigurationBuilder
            3.配置对象：IConfigration
             */

            var source = new Dictionary<string, string>
            {
                ["Name"] = "LNM",
                ["StartDate"] = "2021-11-09",
                ["EndDate"] = "2021-12-31"
            };
            IConfiguration config = new ConfigurationBuilder()
                .Add(new MemoryConfigurationSource { InitialData = source })
                .Build();

            var options = new AppConfigObj(config);

            Console.WriteLine(options.Name);
            Console.WriteLine(options.StartDate);
            Console.WriteLine(options.EndDate);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            var config = new ConfigurationBuilder()
             .Add(new JsonConfigurationSource { Path = "appsettings.json" })
             .Build();
        }
    }
}
