﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Windows.Forms;

namespace WinFormsApp.Study
{
    public partial class StudyRabbitMq : Form
    {
        public StudyRabbitMq()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("发送者启动");
            //创建连接工厂
            IConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = "47.107.103.128",
                Port = 5672,
                UserName = "admin",
                Password = "666666"
            };
            using IConnection connection = connectionFactory.CreateConnection();
            using IModel channel = connection.CreateModel();
            string queueName = string.Empty;

            //队列名称对应不同的接收者
            queueName = "测试队列";
            //声明一个队列
            channel.QueueDeclare(
                queue: queueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
                );
            while (true)
            {
                Console.WriteLine("消息内容");
                string message = Console.ReadLine();
                //消息内容
                byte[] body = Encoding.UTF8.GetBytes(message);
                //发送消息
                channel.BasicPublish(exchange: "", routingKey: queueName, basicProperties: null, body: body);
                Console.WriteLine("成功发送消息：" + message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine("接收者启动");
            //创建连接工厂
            IConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = "47.107.103.128",
                Port = 5672,
                UserName = "admin",
                Password = "666666"
            };
            using IConnection connection = connectionFactory.CreateConnection();
            using IModel channel = connection.CreateModel();
            string queueName = string.Empty;

            queueName = "测试队列";
            //声明一个队列
            channel.QueueDeclare(
                queue: queueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
                );
            //告诉RattitMQ每次向消费者发送一条信息，在消费者未确认之后不能再向它发送消息
            channel.BasicQos(0, 1, false);
            //创建一个消费者对象
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                //Thread.Sleep((new Random().Next(1, 6) * 1000));//随机等待实现能者多劳
                byte[] message = ea.Body.ToArray();//接收消息
                Console.WriteLine("接收者信息为：" + Encoding.UTF8.GetString(message));
                //返回消息确认
                //将消费者监听器的autoAck设置为false关闭自动确认
                //channel.BasicAck(ea.DeliveryTag, false);
            };
            //开启消费者监听器
            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
            Console.ReadKey();
        }
    }
}
