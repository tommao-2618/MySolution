﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp.Study
{
    public partial class StudyThread : Form
    {
        private static readonly object LOCK = new object();
        private static readonly object LOCK1 = new object();
        private static readonly object LOCK2 = new object();
        public StudyThread()
        {
            InitializeComponent();
        }

        #region 公共方法

        /// <summary>
        /// 模拟cup密集型操作
        /// </summary>
        /// <param name="name">名字</param>
        private void Print(string name)
        {
            Console.WriteLine("{0}线程开始，线程id:{1},开始时间：{2}", name, Thread.CurrentThread.ManagedThreadId.ToString("00"), DateTime.Now);
            for (int i = 0; i < 1000000000; i++)
            {
            }

            Console.WriteLine("{0}线程结束，结束时间：{1}", name, DateTime.Now);
        }
        #endregion


        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("主线程开始，线程id:{0},开始时间：{1}", Thread.CurrentThread.ManagedThreadId.ToString("00"), DateTime.Now);
            List<Task> tasks = new List<Task>
            {
                Task.Run(() => this.Print("第一个")),
                Task.Run(() => this.Print("第二个")),
                Task.Run(() => this.Print("第三个")),
            };

            // 阻塞线程
            Task.WaitAll(tasks.ToArray());
            Console.WriteLine("主线程结束，结束时间：{0}", DateTime.Now);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine("主线程开始，线程id:{0},开始时间：{1}", Thread.CurrentThread.ManagedThreadId.ToString("00"), DateTime.Now);

            // 多线程去访问同一个集合，有问题？一般是没问题的，多线程的安全问题都是出在修改同一个对象的
            List<int> intList = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                Task.Run(() =>
                {
                    // intList.Add(i);
                    // 锁
                    lock (LOCK)
                    {
                        intList.Add(i);
                    }
                });
            }

            Thread.Sleep(5000);
            Console.WriteLine(intList.Count); // 不加结果小于10000，有的数据丢失了
            Console.WriteLine("主线程结束，结束时间：{0}", DateTime.Now);

            /*分析：
            List是线性表，在内存上是连续的，假如同一时刻增加一个数据，都是操作同一个内存位置，2个cpu同时发出指令
            内存先执行一个，再执行一个，就出现了覆盖。
            结论：
            多线程的安全问题：一段代码，单线程执行和多线程执行结果不一致，则是出现了线程安全问题。
            加lock锁就能解决安全问题，就是单线程化，保证lock方法块内的任意时刻只有一个线程能进去
            其他线程排队。
            lock原理：锁定一个内存引用地址，所有lock(值)得是引用类型，而且不能为null，null不占据内存
            lock占据的是内存引用。*/
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // 两个task共用同一个锁会出现阻塞，即等一个线程结束，再执行另一个
            for (int i = 0; i < 5; i++)
            {
                Task.Run(() =>
                {
                    lock (LOCK1)
                    {
                        Console.WriteLine("线程一开始，线程id:{0},开始时间：{1}", Thread.CurrentThread.ManagedThreadId.ToString("00"), DateTime.Now);
                        Thread.Sleep(2000);
                        Console.WriteLine("线程一结束，结束时间：{0}", DateTime.Now);
                    }
                });
            }

            for (int i = 0; i < 5; i++)
            {
                Task.Run(() =>
                {
                    lock (LOCK1)
                    {
                        Console.WriteLine("线程二开始，线程id:{0},开始时间：{1}", Thread.CurrentThread.ManagedThreadId.ToString("00"), DateTime.Now);
                        Thread.Sleep(2000);
                        Console.WriteLine("线程二结束，结束时间：{0}", DateTime.Now);
                    }
                });
            }
        }
    }
}
