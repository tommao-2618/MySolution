﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WinFormsApp.StudyData
{
    #region 单例模式
    public class Singleton
    {
        private static Singleton singleton;

        public string MyProperty { get; set; }
        private Singleton()
        {

        }

        /// <summary>
        /// 获取实例-线程非安全模式
        /// </summary>
        /// <returns></returns>
        public static Singleton GetSingleton()
        {
            if (singleton == null)
            {
                singleton = new Singleton();
                singleton.MyProperty = "这是线程不安全的单例";
            }
            return singleton;
        }
    }

    public class ThreadSafeSingleton
    {
        private static ThreadSafeSingleton threadSafeSingleton;

        public string MyProperty { get; set; }

        private static object locker = new object();
        private ThreadSafeSingleton()
        {

        }

        /// <summary>
        /// 获取实例-线程安全模式
        /// </summary>
        /// <returns></returns>
        public static ThreadSafeSingleton GetThreadSafeSingleton()
        {
            if (threadSafeSingleton == null)
            {
                lock (locker)
                {
                    //多线程的情况下需要做两次判断
                    if (threadSafeSingleton == null)
                    {
                        threadSafeSingleton = new ThreadSafeSingleton();
                        threadSafeSingleton.MyProperty = "这是线程安全的单例";
                    }
                }
            }
            return threadSafeSingleton;
        }
    }
    #endregion

    #region 抽象工厂
    public interface IAutoCarMake
    {
        void CreateAutoCar();
    }
    public class RedCar : IAutoCarMake
    {
        public void CreateAutoCar()
        {
            Console.WriteLine("创建红色轿车");
        }
    }

    public class BlueCar : IAutoCarMake
    {
        public void CreateAutoCar()
        {
            Console.WriteLine("创建蓝色轿车") ;
        }
    }

    public class Factory
    {
        public IAutoCarMake FactoryCreateCar(string type)
        {
            switch (type)
            {
                case "red":
                    return new RedCar();
                case "blue":
                    return new BlueCar();
            }
            return null;
        }
    }
    #endregion
}
