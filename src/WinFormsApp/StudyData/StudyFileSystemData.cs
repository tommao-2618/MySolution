﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace WinFormsApp.StudyData
{
    public class StudyFileSystemData
    {
        private readonly IFileProvider _fileProvider;
        public StudyFileSystemData(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }

        public void Dir()
        {
            var indent = -1;
            void Get(string subPath)
            {
                indent++;
                foreach (var fileInfo in _fileProvider.GetDirectoryContents(subPath))
                {
                    Console.WriteLine(new string('\t', indent) + fileInfo.Name);
                    if (fileInfo.IsDirectory)
                    {
                        Get($@"{subPath}\{fileInfo.Name}");
                    }
                }
            }
            Get("");
        }

        public async Task<string> ReadAsync(string path)
        {
            await using var stream = _fileProvider.GetFileInfo(path).CreateReadStream();
            var buffer = new byte[stream.Length];
            await stream.ReadAsync(buffer, 0, buffer.Length);
            return Encoding.Default.GetString(buffer);
        }

        public async void WatchAsync(string path)
        {
            Console.WriteLine(await ReadAsync(path));
            ChangeToken.OnChange(
                () => _fileProvider.Watch(path),
                async () =>
                {
                    Console.Clear();
                    Console.WriteLine(await ReadAsync(path));
                }
                );
        }
    }

    public class AppConfigObj
    {
        public string Name { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public AppConfigObj(IConfiguration configuration)
        {
            Name = configuration["Name"];
            StartDate = configuration["StartDate"];
            EndDate = configuration["EndDate"];
        }
    }

    public class AppConfigObj2
    {
        public string Name { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }
}
