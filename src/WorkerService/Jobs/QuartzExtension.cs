﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace WorkerService
{
    public static class QuartzExtension
    {
        public static IServiceCollection AddQuartzService(this IServiceCollection services)
        {
            // Quartz的工作单元
            services.AddScoped<RegularWork>();

            // Quartz调度中心
            services.AddQuartz(q =>
            {
                // 用于注入
                q.UseMicrosoftDependencyInjectionJobFactory();

                // 基本Quartz调度器、作业和触发器配置
                var jobKey = new JobKey("RegularWork", "regularWorkGroup");
                q.AddJob<RegularWork>(jobKey, j => j.WithDescription("My regular work"));

                q.AddTrigger(t => t
                    .WithIdentity("Trigger")
                    .ForJob(jobKey)
                    .StartNow()
                    .WithSimpleSchedule(x => x.WithInterval(TimeSpan.FromSeconds(10)) // 开始秒数 10s
                    .RepeatForever()));
            });

            // ASP.NET核心托管-添加Quartz服务
            services.AddQuartzServer(options =>
            {
                // 关闭时，我们希望作业正常完成
                options.WaitForJobsToComplete = false;
            });
            return services;
        }
    }
}
