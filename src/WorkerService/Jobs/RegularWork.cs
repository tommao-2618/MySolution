﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using StackExchange.Redis;

namespace WorkerService
{
    public class RegularWork : IJob
    {
        private readonly ILogger<RegularWork> _logger;
        private readonly IDatabase _redisDb;

        public RegularWork(
            ILogger<RegularWork> logger,
            RedisService redisService)
        {
            _logger = logger;
            _redisDb = redisService.GetDatabase();
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var now = DateTime.Now;
            var key = now.ToString("yyyyMMdd:HHmmss");
            var value = now.ToString("yyyy-MM-dd HH:mm:ss");
            await _redisDb.StringSetAsync(key, value, TimeSpan.FromMinutes(1));
        }
    }
}
